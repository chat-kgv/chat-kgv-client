import Options from "./Options/Options";
import Searching from "./Searching/Searching";
import MessagesTemplate from "./Messages/MessagesTemplate/MessagesTemplate";
import MessagesList from "./Messages/MessagesList/MessagesList";
import './LeftMain.scss'
const LeftMain = () => {
    return (
        <div className="leftMain">
            <Searching/>
            <Options/>
            <MessagesList/>

            {/* <button>Invite members</button> */}
        </div>
    )
}

export default LeftMain;